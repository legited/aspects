import { AspectsPage } from './app.po';

describe('aspects App', () => {
  let page: AspectsPage;

  beforeEach(() => {
    page = new AspectsPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
