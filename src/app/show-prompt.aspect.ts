export function showPromptDecorator() {
  return function (target: any, propertyKey: string, descriptor: PropertyDescriptor) {
    const method = descriptor.value;

    descriptor.value = function (...args: any[]) {
      const consent = window.prompt('aspect yo');
      // tslint:disable-next-line:curly
      if (consent) method.apply(this, args);
    };

  };
}

export function showPromptAspect(method: Function) {
  return function (...args: any[]) {
    const consent = window.confirm('aspect yo');
    // tslint:disable-next-line:curly
    if (consent) method.apply(this, args);
  };
}
