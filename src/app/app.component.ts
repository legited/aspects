
import { Component, ChangeDetectionStrategy } from '@angular/core';
// import { showPromptDecorator } from './show-prompt.aspect';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppComponent {
  private title = 'app works!';

  /*
     @showPromptDecorator() - the application will control if a prompt needs to be shown
     for this action or not. This component should not be aware of it
   */
  public toggleTitle() {
    this.title === 'another title' ? (this.title = 'app works!') : (this.title = 'another title');
  }
}
