import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { showPromptAspect } from './show-prompt.aspect';

/*
  Composition root defines aspects on actions.
  In this case we want to ask confirmation to toggle text.
 */
const toggleTitle = AppComponent.prototype.toggleTitle;
AppComponent.prototype.toggleTitle = showPromptAspect(toggleTitle);

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
